<?php 
    session_start();

   function makeRedirect($pageName){
        header('Location: '.$pageName.' ');
    }    
  
  function checkLogin($userLogin, $userPassword){
        $user = getUser($userLogin);
        if(!empty($user) && $user["password"] == $userPassword)
            return true;
         else
            return false;
   }
 
 function getUser($userLogin){
    if(!file_exists (__DIR__.'/persons_data/persons.json')) return [];
    $jsonData=file_get_contents(__DIR__.'/persons_data/persons.json'); 
    if(!isset($jsonData)) return [];
    $usersData=json_decode($jsonData,true);
    if(!array_key_exists ($userLogin , $usersData )) return [];
    else {
        $user = array(
             'password'=>$usersData[$userLogin]['password'],
             'is_admin'=>$usersData[$userLogin]['is_admin']
        );
    }
    return $user;
}

function getJson($fileName){
    if(!file_exists ($fileName)) return [];
    $jsonData = file_get_contents($fileName);  
    $questionArray = json_decode($jsonData, true); 
    return $questionArray;
}

function getTestNamefromGet($getParam){
     $arr = glob(__DIR__ . '/tests/*'.$getParam.'_.json');
     if(count($arr) == 0) return '';
     return $arr[0];
}

function getTestNamefromURL(){
    $test = getTestFileNamefromURL();
    $name_len = strlen ( $test );
    $start = strpos($test, 'tests')+ strlen (  'tests/' );    
    $name = explode('_', substr($test, $start));
    return $name[0];
}

function getTestFileNamefromURL(){
    $url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY);
    $testNumber = explode('=', $url);
    $tests = glob(__DIR__ . '/tests/*_'.$testNumber[1].'_.json');     
    if(count($tests) == 0) return '';
    return $tests[0] ;
}
    
	


