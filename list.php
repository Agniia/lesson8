<?php
    require_once(__DIR__.'/functions.php');       
    if(!empty($_GET) && array_key_exists('action', $_GET) && array_key_exists('test', $_GET)){
        if(($_SESSION['is_admin'] == '1') && $_GET['action'] === 'delete'){
            $filename = getTestNamefromGet( $_GET['test'] );
             unlink ($filename);
             makeRedirect('list.php');
        }
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    </head>
    <body> 
    <?php
        if(isset($_SESSION['is_admin']) && !empty($_SESSION['is_admin'])) {
           if($_SESSION['is_admin'] == '1'){
                echo '<a href="admin.php">Добавить тест</a>';
            }
        }else if(!isset($_SESSION['user']) && empty($_SESSION['user'])){
            http_response_code(403);
            die;
        }
    ?>
    <h1 style="width: 800px; margin: 50px auto 40px; text-align:center">Список доступных тестов</h1>
 <table class="table" style="width: 800px; margin:auto">
    <thead>
     <tr>
      <th scope="col">Название теста</th>
      <th scope="col">Тест</th>
       <?php if($_SESSION['is_admin'] == '1'){
                echo '<th scope="col">Опции</th>';
       } ?>
     </tr>
   </thead>
    <?php
        $tests = glob(__DIR__ . '/tests/*.json');
        foreach($tests as $test){
            $name_len = strlen ( $test );
            $start = strpos( $test, 'tests')+ strlen (  'tests/' );    
            $name = explode('_', substr($test , $start));
            echo '<tr>';
            echo '<td scope="col">'.$name[0].'</td>';
            echo '<td scope="col"><a href="test.php?test='.$name[2].'">Перейти к тесту</a></td>';
            if($_SESSION['is_admin'] == '1'){
                echo '<td scope="col"><a href="?test='.$name[2].'&action=delete">Удалить тест</a></td>';
            }
            echo '</tr>';
        }
    ?>
    </table>
    </body>
</html>    
	


