<?php 
    require_once(__DIR__.'/functions.php');   
    if(!empty($_POST)){
        if(array_key_exists('auth', $_POST)){                  
            if(checkLogin($_POST['login'], $_POST['password'])){
                $_SESSION['user'] = $_POST['login'];
                $_SESSION['password'] = $_POST['password'];
                $_SESSION['is_admin'] = '1';
            }     
        }else if(array_key_exists('enter', $_POST)){
                $_SESSION['user'] = $_POST['name'];
                $_SESSION['is_admin'] = '0';
        }
        makeRedirect('list.php');
    }
//var_dump( $_SESSION);
//session_destroy();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    </head>
    <body> 
        <h1 style="text-align: center; margin-bottom: 50px">Приветствуем!</h1>
        <div style="display: flex; justify-content: space-between;" class="row justify-content-around">
            <div class="col-lg-4 col-md-4 col-12">
                <h2 style="text-align: center; margin-bottom: 30px">Авторизуйтесь</h2>
                    <form action="index.php" method="post">
                      <div class="form-group">
                        <label for="login">Login</label>
                        <input type="text" class="form-control" name="login" id="login" placeholder="Введите Ваш login">
                      </div>
                      <div class="form-group">
                        <label for="password">Пароль</label>
                        <input type="password" class="form-control" name="password" id="password" placeholder="Введите пароль">
                      </div>
                      <button type="submit" name="auth" class="btn btn-primary">Авторизоваться</button>
                    </form>
            </div>
            <div class="col-lg-4 col-md-4 col-12">
                <h2 style="text-align: center; margin-bottom: 30px">Войдите как гость</h2>
                    <form action="index.php" method="post">
                      <div class="form-group">
                        <label for="name">Ваше имя</label>
                        <input type="text" class="form-control" name="name"  id="name" placeholder="Введите Ваше имя">
                      </div>                      
                      <button type="submit"  name="enter" class="btn btn-primary">Войти как гость</button>
                   </form>
            </div>
        </div>
    </body>
</html>    
	


